package pl.test.asyncjpapredicateandarch.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.test.asyncjpapredicateandarch.model.Person;

import java.util.List;

import static jakarta.persistence.criteria.JoinType.LEFT;

@Service
@RequiredArgsConstructor
public class PersonFinder {

    private final EntityManager entityManager;

    public List<Person> findPersonsByAge(int age) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);

        Root<Person> personRoot = criteriaQuery.from(Person.class);

        Predicate agePredicate = criteriaBuilder.equal(personRoot.get("age"), age);

        criteriaQuery.where(agePredicate);

        TypedQuery<Person> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }
}
