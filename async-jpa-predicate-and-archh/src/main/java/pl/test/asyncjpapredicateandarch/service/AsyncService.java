package pl.test.asyncjpapredicateandarch.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
@Slf4j
public class AsyncService {

    @Async
    public Future<String> doAsyncTask() throws InterruptedException {
        log.info("Robi się w tle");
        Thread.sleep(20000);
        log.info("wynik");
        return new AsyncResult<>("5");
    }

}
