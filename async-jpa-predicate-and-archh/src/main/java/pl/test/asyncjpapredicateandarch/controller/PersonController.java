package pl.test.asyncjpapredicateandarch.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.test.asyncjpapredicateandarch.model.Person;
import pl.test.asyncjpapredicateandarch.service.PersonFinder;

import java.util.List;

@RequestMapping("/api/person")
@RestController
@RequiredArgsConstructor
public class PersonController {

    private final PersonFinder personFinder;

    @GetMapping("/find")
    public List<Person> persons(@RequestParam Integer age) {
        return personFinder.findPersonsByAge(age);
    }
}
