package pl.test.asyncjpapredicateandarch.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.test.asyncjpapredicateandarch.service.AsyncService;

import java.util.concurrent.ExecutionException;

@RequestMapping("/api/test")
@RestController
@Slf4j
@RequiredArgsConstructor
public class AsyncController {

    private final AsyncService asyncService;

    @GetMapping("/synchronized")
    public String synchronizedd(@RequestParam String number) throws InterruptedException {
        val threadName = Thread.currentThread().getName();
        log.info("Before synchronized thread start: " + threadName);
        String result;
        synchronized (number.intern()) {
            val threadNameSynchronized = Thread.currentThread().getName();
            log.info("Thread start: " + threadNameSynchronized);
            Thread.sleep(10000);
            result = "result";
            log.info("Thread end: " + threadNameSynchronized);
        }
        log.info("After synchronized thread start: " + threadName);

        return result;
    }

    @GetMapping("/async")
    public String async() throws InterruptedException, ExecutionException {
        val threadName = Thread.currentThread().getName();
        log.info("Before async thread start: " + threadName);
        val futureResult = asyncService.doAsyncTask();
        log.info("After async thread start: " + threadName);

        val result = futureResult.get();
        log.info("result" + result);
        return result;
    }
}
