package pl.test.asyncjpapredicateandarch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsyncJpaPredicateAndArchApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsyncJpaPredicateAndArchApplication.class, args);
	}

}
