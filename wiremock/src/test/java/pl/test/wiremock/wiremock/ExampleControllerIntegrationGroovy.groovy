package pl.test.wiremock.wiremock

import org.springframework.test.context.TestPropertySource
import pl.test.wiremock.wiremock.common.IntegrationTestJunitGroovy

@TestPropertySource(locations = "classpath:application-test.properties")
class ExampleControllerIntegrationGroovy extends IntegrationTestJunitGroovy {

    def "integration test using wiremock"() {
        given:
        def expectedResponse = "{\"sample\":\"value\"}";
        when:
        def response = restTemplate.getForObject("http://localhost:" + port + "/api/test", String.class);
        then:
        Objects.equals(response, expectedResponse);
    }
}
