package pl.test.wiremock.wiremock;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;
import pl.test.wiremock.wiremock.common.IntegrationTestJunit;

import java.util.Objects;

@TestPropertySource(locations = "classpath:application-test.properties")
class ExampleControllerIntegrationJunit extends IntegrationTestJunit {

    @Test
    public void testMyControllerEndpoint() {
        String response = restTemplate.getForObject("http://localhost:" + port + "/api/test", String.class);
        String expectedResponse = "{\"sample\":\"value\"}";
        assert Objects.equals(response, expectedResponse);
    }

}
