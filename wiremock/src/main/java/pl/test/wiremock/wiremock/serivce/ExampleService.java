package pl.test.wiremock.wiremock.serivce;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class ExampleService {

    private final RestTemplate restTemplate;
    @Value("${test.api.url}")
    private String apiUrl;

    public String makeHttpRequest() {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(apiUrl, String.class);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return responseEntity.getBody();
        } else {
            return "Błąd podczas żądania HTTP: " + responseEntity.getStatusCode();
        }
    }
}
