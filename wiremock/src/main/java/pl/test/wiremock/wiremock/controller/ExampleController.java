package pl.test.wiremock.wiremock.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.test.wiremock.wiremock.serivce.ExampleService;

@RestController
@RequestMapping("/api/test")
@RequiredArgsConstructor
public class ExampleController {

    private final ExampleService exampleService;

    @GetMapping()
    public String makeHttpRequest() {
        return exampleService.makeHttpRequest();
    }
}
